<?php

namespace App\CalculateDistance;

use App\CalculateDistance\Interfaces\DistanceCalculationStrategy;
use App\CalculateDistance\Traits\DistanceCalculate;

class PositionStackCalculationStrategy implements DistanceCalculationStrategy
{
    use DistanceCalculate;

    /**
     * Calculate distances using the PositionStack API and store the results in a CSV file.
     *
     * @param array $distances Array of distances
     *
     * @return string Result message indicating the success of distance calculation and CSV file creation
     */
    public function calculateDistance(array $addresses): array
    {
        $achieveCoords = $this->getAchieveCoordinates();
        return $this->calculateDistances($addresses, $achieveCoords);
    }

    /**
     * Get the coordinates of the Achieve headquarters.
     *
     * @return array Array containing latitude and longitude of the Achieve headquarters
     */
    private function getAchieveCoordinates(): array
    {
        // Retrieve the coordinates of the Achieve headquarters from the configuration or database
        return [
            'lat' => 51.689611,
            'lng' => 5.309674
        ];
    }

    /**
     * Calculate distances between addresses and Achieve headquarters.
     *
     * @param array $addresses Array of addresses
     * @param array $achieveCoords Array containing latitude and longitude of the Achieve headquarters
     *
     * @return array Array of calculated distances
     */
    private function calculateDistances(array $addresses, array $achieveCoords): array
    {
        $calculatedDistances = [];

        foreach ($addresses as $address) {
            $coords = $this->geocodeAddress($address['address']);
            $distance = $this->calculateDistanceWithKM($achieveCoords['lat'], $achieveCoords['lng'], $coords['lat'], $coords['lng']);

            $calculatedDistances[] = [
                'name' => $address['name'],
                'address' => $address['address'],
                'distance' => $distance
            ];
        }

        return $calculatedDistances;
    }

    /**
     * Geocode the given address using the PositionStack API.
     *
     * @param string $address Address to geocode
     *
     * @return array Array containing latitude and longitude of the geocoded address
     */
    private function geocodeAddress(string $address): array
    {
        $apiKey = env('POSITIONSTACK_API_KEY');
        $url = 'http://api.positionstack.com/v1/forward?access_key=' . $apiKey . '&query=' . urlencode($address);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        $data = json_decode($response, true);
        $lat = $data['data'][0]['latitude'];
        $lng = $data['data'][0]['longitude'];

        return [
            'lat' => $lat,
            'lng' => $lng
        ];
    }
}