<?php

namespace App\CalculateDistance;

use App\CalculateDistance\Interfaces\DistanceCalculationStrategy;

use League\Csv\Writer;

class DistanceCalculator
{
    public function __construct(private DistanceCalculationStrategy $distanceCalculationStrategy){}

    public function calculateDistance(array $addresses): string 
    {
        $calculatedDistances = $this->distanceCalculationStrategy->calculateDistance($addresses);

        $sortedDistances = $this->sortDistances($calculatedDistances);

        $this->writeDistancesToCsv($sortedDistances);

        return "Distances calculated and saved to distances.csv";
    }


    /**
     * Sort distances in ascending order.
     *
     * @param array $distances Array of distances
     *
     * @return array Sorted array of distances
     */
    private function sortDistances(array $distances): array
    {
        usort($distances, function ($a, $b) {
            return $a['distance'] - $b['distance'];
        });

        return $distances;
    }

    /**
     * Write distances to a CSV file.
     *
     * @param array $distances Array of distances
     *
     * @return void
     */
    private function writeDistancesToCsv(array $distances): void
    {
        $csv = Writer::createFromPath(public_path('distances.csv'), 'w+');
        $csv->insertOne(['Sortnumber', 'Distance', 'Name', 'Address']);

        $sortNumber = 1;
        foreach ($distances as $distance) {
            $csv->insertOne([$sortNumber, $distance['distance'] . ' km', $distance['name'], $distance['address']]);
            $sortNumber++;
        }
    }
}