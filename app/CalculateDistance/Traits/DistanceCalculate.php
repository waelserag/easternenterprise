<?php

namespace App\CalculateDistance\Traits;

trait DistanceCalculate
{    
    /*
    * Calculate Distance By KM
    * @param float $lat1 (latitude 1)
    * @param float $lng1 (longitude 1)
    * @param float $lat2 (latitude 2)
    * @param float $lng2 (longitude 2)
    * @return float
    */
    private function calculateDistanceWithKM(float $lat1, float $lng1, float $lat2, float $lng2): float
    {
        $earthRadius = 6371; // Radius of the earth in kilometers

        $latDiff = deg2rad($lat2 - $lat1);
        $lngDiff = deg2rad($lng2 - $lng1);

        $a = sin($latDiff / 2) * sin($latDiff / 2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($lngDiff / 2) * sin($lngDiff / 2);

        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $distance = $earthRadius * $c;

        return round($distance, 2);
    }
}
