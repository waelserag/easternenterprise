<?php

namespace App\CalculateDistance\Interfaces;

interface DistanceCalculationStrategy
{
    public function calculateDistance(array $addresses): array;
}