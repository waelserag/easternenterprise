<?php

namespace App\Console\Commands;

use App\CalculateDistance\DistanceCalculator;
use App\CalculateDistance\PositionStackCalculationStrategy;
use Illuminate\Console\Command;

class CalculateDistances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'distances:calculate {addresses*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resolve addresses	to geolocations	using an API and	
    calculate the distance in kilometres from each address to the Achieve headquarters.	
    Then be	stored in distances.csv';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $distanceCalculator = new DistanceCalculator(new PositionStackCalculationStrategy());
        $distanceCalculator->calculateDistance($this->argument('addresses'));
    }
}
